/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package apim15.algoanalyst;

import java.util.Vector;

/**
 *
 * @author User
 */
public class Detector implements I_Named{
    private Vector<I_DetectorValue> data = new Vector();
    private String name;

    public void setName(String name) {
        this.name = name;
        }
    I_DetectorValue proto;
    public Detector(){}

    public Detector(String name, I_DetectorValue proto){
        this.proto = proto;
        this.name = name;
        }
    public void add(I_DetectorValue val){
        data.add(val);
        }
    @Override
    public String getName() {
        return name;
        }
    public I_DetectorValue getProto() {
        return proto;
        }
    public Vector<I_DetectorValue> getData(){
        return data;
        }
}
