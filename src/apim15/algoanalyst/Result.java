/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package apim15.algoanalyst;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.util.Vector;

/**
 *
 * @author User
 */
public class Result implements I_Named, I_File {
    public Vector<I_DetectorValue> protos; //прототипы детекторов(характеризует тип измеряемого параметра)
    public Vector<Detector> results;       //результирующие детекторы
    public Vector<Integer>  dimensions;    //разрешения(длина тестового набора) детекторов
    private String Name="";

    public Result(Vector<I_DetectorValue> protos) {
        this.protos = protos;
        results = new Vector();
        dimensions = new Vector();
        for(int i = 0;i<protos.size();i++){
            results.add(new Detector(Name+"-"+i,protos.get(i)));
        }
    }

    public Result(Vector<I_DetectorValue> protos, Vector<Detector> results,
                  Vector<Integer>  dimensions, String Name)
    {
        this.protos = new Vector<I_DetectorValue>(protos);
        this.results = new Vector<Detector>(results);
        this.dimensions = new Vector<Integer>(dimensions);
        this.Name = Name;
    }

    public I_DetectorValue[] createDetectors(){
        I_DetectorValue[] detectors = new I_DetectorValue[protos.size()];
        for(int i=0;i<protos.size();i++){
            detectors[i] = protos.get(i).cloneDetector();
        }
        return detectors;
    }

    public  void addDetectors(I_DetectorValue[] vals){
        for(int i=0;i<protos.size();i++){
            results.get(i).add(vals[i]);
        }
    }

    @Override
    public String getName() {
        return Name;
    }

    @Override
    public void save(DataOutputStream out) throws IOException {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void load(DataInputStream in) throws IOException {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
}
