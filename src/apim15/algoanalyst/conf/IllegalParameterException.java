package apim15.algoanalyst.conf;

/**
 * Thrown by implementation of I_Configurable to indicate that the value of a parameter
 * is logically incorrect, e.g. array size is less than 0.
 * Not to be confused with ParameterFormat exception which implies only formatting error.
 * 
 * @author eremin
 */
public class IllegalParameterException extends Exception {

    private String parameterName;

    /**
     * Constructs an IllegalParameter exception.
     * @param message the detail message
     * @param parameterName name of the illegal parameter
     */
    public IllegalParameterException(String message, String parameterName) {
        super(message);
        this.parameterName = parameterName;
    }

    /**
     * Return the name of the illegal parameter.
     * @return name of the illegal parameter
     */
    public String getParameterName() {
        return parameterName;
    }
}
