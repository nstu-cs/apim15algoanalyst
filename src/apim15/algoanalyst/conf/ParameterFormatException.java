package apim15.algoanalyst.conf;

/**
 * Thrown by implementation of I_Configurable to indicate that the value of a parameter
 * has wrong format thus cannot be parsed.
 *
 * @author eremin
 */
public class ParameterFormatException extends Exception {

    private String parameterName;

    /**
     * Constructs a ParameterFormat exception.
     * @param message the detail message
     * @param parameterName name of the ill-formatted parameter
     */
    public ParameterFormatException(String message, String parameterName) {
        super(message);
        this.parameterName = parameterName;
    }

    /**
     * Return the name of the ill-formatted parameter.
     * @return name of the ill-formatted parameter
     */
    public String getParameterName() {
        return parameterName;
    }
}
