package apim15.algoanalyst.conf;

/**
 * Represents general interface of configuration for objects.
 * Provides an alternative for constructor arguments / getters & setters.
 * Main intention behind this interface is to allow gui to have object configuration
 * implementation that is agnostic of actual java interface of an object and therefore is general.
 *
 * @author eremin
 */
public interface I_Configurable {
    /**
     * Return value represents two things.
     * Firstly, returned set represents configuration specification, i.e. lists
     * all parameters that are available for configuration.
     * Secondly, it contains current values of parameters. For a freshly created
     * object they must be defaults.
     *
     * @return list of parameters with their current values
     */
    ParameterSet getParams();

    /**
     * Sets parameters for an object.
     * It is implementor's responsibility to parse values, thus this method can throw the exception.
     * If set contains unexpected parameters, they will be ignored.
     * Set can contain only those parameters that are required to change (i.e. subset of what
     * is returned from getParams()).
     *
     * @param params
     * @throws ParameterFormatException
     */
    void setParams(ParameterSet params) throws ParameterFormatException, IllegalParameterException;
}
