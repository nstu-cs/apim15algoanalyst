package apim15.algoanalyst.conf;

import java.util.LinkedList;
import java.util.List;

/**
 * Set of parameters available for configuration in implementation of I_Configurable.
 *
 * @author eremin
 */
public class ParameterSet {

    /**
     * Single parameter.
     */
    public static class Parameter {
        private String name, description, value;

        /**
         * Constructs a Parameter.
         *
         * @param name name of the parameter. Intended to be shown to a user but also
         *             identifies the parameter internally.
         * @param description short description to show to a user
         * @param value value of the parameter
         */
        public Parameter(String name, String description, String value) {
            this.name = name;
            this.description = description;
            this.value = value;
        }

        public Parameter(String name, String value) {
            this.name = name;
            this.value = value;
            this.description = "";
        }

        /**
         * Sets the value for the parameter.
         * Intended to be used by gui implementation.
         *
         * @param value value of the parameter
         */
        public void setValue(String value) {
            this.value = value;
        }

        public String getName() {
            return name;
        }

        public String getDescription() {
            return description;
        }

        public String getValue() {
            return value;
        }
    }
    
    private List<Parameter> params = new LinkedList<>();

    public ParameterSet() {}

    // Probably need to check parameter name for uniqueness, but let's keep it simple for now
    public ParameterSet add(Parameter param) {
        params.add(param);
        return this;
    }

    public List<Parameter> getParameters() {
        return params;
    }
}
