/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package apim15.algoanalyst;

import apim15.algoanalyst.data.I_DataElement;


/**
 *
 * @author ark
 */
public interface I_DetectorValue extends I_DataElement{
    public double getDouble();
    public int getInt();
    public boolean hasDouble();            
    public boolean hasInt(); 
    public I_DetectorValue cloneDetector();
}
