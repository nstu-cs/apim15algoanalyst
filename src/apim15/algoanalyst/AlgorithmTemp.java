package apim15.algoanalyst;

import apim15.algoanalyst.test.*;
import apim15.algoanalyst.data.*;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Random;
import java.util.Vector;

/**
 *
 * @author Kachurin
 */
//

public class AlgorithmTemp extends A_Algorithm //Алгоритм с реализацией трёх типов сортировок
{

    public class DVInt implements I_DetectorValue //Целочисленное значение замера для хранения в детекторе
    {
        private int Val;//начение
        private String name;//Имя
        public DVInt(){this.Val=0; this.name=null;}
        public DVInt(String _name){this.Val=0; this.name=_name;}
        public DVInt(int _val){this.Val=_val; this.name=null;}
        public DVInt(int _val, String _name){this.Val=_val; this.name=_name;}
        @Override
        public void inc(I_DataElement step)
        {
            this.Val+=((DVInt)step).getInt();
        }
        @Override
        public void inc(Random step, I_DataElement MIN, I_DataElement MAX)
        {
            int mn=((DVInt)MIN).getInt(),mx=((DVInt)MAX).getInt();
            this.Val+=(mx-mn)*step.nextDouble()+mn;
        }
        @Override
        public void parse(String ss){try{this.Val=Integer.parseInt(ss);}catch(NumberFormatException NFE){}}//Получает значение из строки
        @Override
        public String toString(){return String.valueOf(this.Val);}//Выводит значение в виде строки
        @Override
        public I_DetectorValue clone()//Клонирование с сохранением значения
        {
            return this.clone();
        }
        @Override
        public int cmp(I_DataElement secondElement)//Сравнение (>0 - текущий элемен больше secondElement, <0 - меньше, ==0 - равен, при неверном типе всегда считается как больший)
        {
            if ((secondElement instanceof DVInt)==false)
            {
                return 1;
            }
            DVInt sE = (DVInt)secondElement;
            int isE = sE.getInt();
            return this.Val-isE;
        }
        @Override
        public boolean equal(I_DataElement secondElement)//Сравнение значений (неверный тип никогда не равен)
        {
            if ((secondElement instanceof DVInt)==false)
            {
                return false;
            }
            DVInt sE = (DVInt)secondElement;
            int isE = sE.getInt();
            return (this.Val==isE);
        }
    
        public void setInt(int _val){this.Val=_val;}//Устанавливает целочисленное значение
    
        @Override
        public double getDouble(){return Double.NaN;}//Не догступно
        @Override
        public int getInt(){return this.Val;}//Возврат текущего значения
        @Override
        public boolean hasDouble(){return false;}//Вещественного числа не содержит
        @Override      
        public boolean hasInt(){return true;}//Целочисленное значение содержит
        @Override
        public I_DetectorValue cloneDetector()//Клонирование (то же самое, что и DVInt.clone())
        {
            return (I_DetectorValue)this.clone();
        }
        @Override
        public void save(DataOutputStream out) throws IOException//Срхранение в потоке
        {
            out.writeInt(Val);
        }
        @Override
        public void load(DataInputStream in) throws IOException//Чтение из потока
        {
            this.Val=in.readInt();
        }
    
        @Override
        public String getTypeName()//Имя типа (в данном случае - класса)
        {
            return this.getClass().getName();
        }
        @Override
        public String getName()//Имя значения
        {
            return this.name;
        }
    };
    
    public enum Algs {SelectSort, BubbleSort, InsertSort};//перечисление доступных алгоритмов - выбором, пузырьковая, вставками
    
    static class SortAlgExecute//Статический класс, используемый AlgoritmTemp для исполнения различных сортировок в execute
    {
        public static void executeSelectSort(I_DataElement[] data, I_DetectorValue[] detectors)//Сортировка выбором
        {
            I_DataElement[] W = new I_DataElement[data.length];
            for (int i=0; i!=W.length; i++)
                W[i]=data[i];
            long time = System.currentTimeMillis();
            int iters=0, comparisons=0, exchanges=0;
            
            //=======================================
            for (int i=0; i!=W.length-1; i++)
            {
                exchanges++;
                iters++;
                int p=i;
                for (int j=i+1; j!=W.length; j++)
                {
                    comparisons++;
                    if (W[p].cmp(W[j])>0) p=j;
                }
                I_DataElement t=W[i]; W[i]=W[p]; W[p]=t;
            }
            //=======================================
           
            time = System.currentTimeMillis() - time;
            DVInt dvi;
            dvi = (DVInt)detectors[0]; dvi.setInt((int)time);
            dvi = (DVInt)detectors[1]; dvi.setInt(iters);
            dvi = (DVInt)detectors[2]; dvi.setInt(comparisons);
            dvi = (DVInt)detectors[3]; dvi.setInt(exchanges);
        }
        
        
        public static void executeBubbleSort(I_DataElement[] data, I_DetectorValue[] detectors)//Сортировка пузырьковая
        {
            I_DataElement[] W = new I_DataElement[data.length];
            for (int i=0; i!=W.length; i++)
                W[i]=data[i];
            long time = System.currentTimeMillis();
            boolean cnt=true;
            int iters=0, comparisons=0, exchanges=0;
            
            //=======================================
            while (cnt)
            {
                iters++;
                cnt=false;
                for (int i=0; i!=W.length-1; i++)
                {
                    comparisons++;
                    if (W[i].cmp(W[i+1])>0)
                    {
                        exchanges++;
                        I_DataElement t=W[i]; W[i]=W[i+1]; W[i+1]=t;
                        cnt=true;
                    }
                }
            }
            //=======================================
            
            time = System.currentTimeMillis() - time;
            DVInt dvi;
            dvi = (DVInt)detectors[0]; dvi.setInt((int)time);
            dvi = (DVInt)detectors[1]; dvi.setInt(iters);
            dvi = (DVInt)detectors[2]; dvi.setInt(comparisons);
            dvi = (DVInt)detectors[3]; dvi.setInt(exchanges);
        }
        
        
        
        public static void executeInsertSort(I_DataElement[] data, I_DetectorValue[] detectors)//Сортировка вставками
        {
            I_DataElement[] W = new I_DataElement[data.length];
            for (int i=0; i!=W.length; i++)
                W[i]=data[i];
            long time = System.currentTimeMillis();
            int iters=0, comparisons=0, exchanges=0;
            
            //=======================================
            for (int i=1; i!=W.length; i++)
            {
                int p=i;
                for (int j=i-1; j!=W.length; j++)
                {
                    comparisons++;
                    if (W[p].cmp(W[j])>=0) {p=j; break;}
                }
                I_DataElement t=W[i]; W[i]=W[p]; W[p]=t;
                exchanges++;
                iters++;
            }
            //=======================================
           
            time = System.currentTimeMillis() - time;
            DVInt dvi;
            dvi = (DVInt)detectors[0]; dvi.setInt((int)time);
            dvi = (DVInt)detectors[1]; dvi.setInt(iters);
            dvi = (DVInt)detectors[2]; dvi.setInt(comparisons);
            dvi = (DVInt)detectors[3]; dvi.setInt(exchanges);
        }
    };
    
    
    
    
    
    

//===============================================================================================================================
    Algs Alg;//Выбранный алгоритм
    public AlgorithmTemp(String name){super(name); Alg=Algs.SelectSort;}//Конструктор с заданием имени
    public void setSortAlg(Algs _A){this.Alg = _A;}//Выбор алгоритма
    @Override
    public Result executePlan(TestPlan testPlan)//Выполнение плана
    {
        if (testPlan==null)
            return null;
        int c_detectors = 4,c_iters;//Количество детекторов и наборов данных
        
        ArrayList<TestCase> TC = (ArrayList<TestCase>)testPlan.getTestCases();//Наборы данных
        c_iters = TC.size();

        Vector<I_DetectorValue> protos = this.createDetectors();
        for (int i=0; i!=c_detectors; i++)
            protos.add(new DVInt());//Прототипы как приметы типов значение, от которых будут копироваться остальные значения детекторов для использования
        
        String[] Dnames = new String[c_detectors];//Имена детекторов
        Dnames[0] = "Время исполнения";
        Dnames[1] = "Количество итераций";
        Dnames[2] = "Количество сравнений";
        Dnames[3] = "Количество обменов";
        
        I_DetectorValue[] dvs = new I_DetectorValue[c_detectors];//По 4 измеряемых значения
        Vector<Detector> detectors = new Vector<>();
        for (int i=0; i!=c_detectors; i++)//Создание детекторов
        {
            detectors.add(new Detector(Dnames[i], protos.elementAt(i)));
        }
        
        Vector<Integer> dims = new Vector<>();//Количество элементов в каждом наборе данных
        
        
        ArrayList<I_DataElement> D;
        for (int i=0; i!=c_iters; i++)
        {
            D=(ArrayList<I_DataElement>)TC.get(i).getTestData();//Набор данных
            dims.add(i, D.size());//Количество сортируемых элементов в очередном наборе данных
            for (int j=0; j!=c_detectors; j++)
            {
                dvs[j]=protos.get(j).cloneDetector();//Создание объектов для последующего сохранения в них значения каджого из 4-х замеров
            }
            this.execute((I_DataElement[])D.toArray(), dvs);//Выполнение алгоритма сортировки, заданного переменной текущего класса Alg
            for (int j=0; j!=c_detectors; j++)
            {
                detectors.get(j).add(dvs[j]);//Пополнение содержимого каджого детектора на 1 значение
            }
        }
        
        Result R = new Result(protos,detectors,dims,this.Alg.toString());//Результаты (прототипы, детекторы (4 шт.), размерности входных наборов данных и имя алгоритма сортировки)
        return R;
    }
    @Override
    public void execute(I_DataElement[] data, I_DetectorValue[] detectors)
    {    
        if (this.Alg == Algs.SelectSort) SortAlgExecute.executeSelectSort(data,detectors);
        if (this.Alg == Algs.BubbleSort) SortAlgExecute.executeBubbleSort(data,detectors);
        if (this.Alg == Algs.InsertSort) SortAlgExecute.executeInsertSort(data,detectors);
    }
    
    @Override
    public Vector<I_DetectorValue> createDetectors()//Создание детекторов
    {
        Vector<I_DetectorValue> V = new Vector<I_DetectorValue>();
        return V;
    }
}