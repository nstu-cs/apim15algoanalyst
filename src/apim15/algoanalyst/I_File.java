/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package apim15.algoanalyst;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;

/**
 *
 * @author User
 */
public interface I_File {
    public void save(DataOutputStream out) throws IOException;
    public void load(DataInputStream in) throws IOException;
}
