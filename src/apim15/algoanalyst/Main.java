/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package apim15.algoanalyst;

import apim15.algoanalyst.data.FloatElement;
import apim15.algoanalyst.data.I_DataElement;
import apim15.algoanalyst.data.I_SequenceGenerator;
import apim15.algoanalyst.data.IntegerElement;
import apim15.algoanalyst.data.LinearGenerator;
import apim15.algoanalyst.data.RandomGenerator;
import apim15.algoanalyst.data.StringElement;
import apim15.algoanalyst.test.ArithmeticDimensionGenerator;
import apim15.algoanalyst.test.I_DimensionGenerator;
import apim15.algoanalyst.test.TestCase;
import apim15.algoanalyst.test.TestPlan;

/**
 *
 * @author Admin
 */
public  class Main {
    
    public static void p(String s)
    {
        System.out.println(s);
    }
    
    public static void main(String[] args) {
        I_SequenceGenerator sg = new RandomGenerator(new StringElement("1.5f"), new StringElement("fsdfg"), new StringElement("sdfgsf"));
        I_DimensionGenerator dg = new ArithmeticDimensionGenerator();
        
        TestPlan plan = new TestPlan("test", 10, sg, dg);
        for(TestCase tc : plan.getTestCases())
        {
            p("next test case; size: " + tc.getTestData().size());
            for(I_DataElement el: tc.getTestData())
                p("\t" + el.toString());
        }
    }
}
