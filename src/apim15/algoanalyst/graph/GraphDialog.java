package apim15.algoanalyst.graph;

import javax.swing.*;
import javax.swing.border.LineBorder;
import java.awt.*;
import java.awt.event.*;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Vector;

import apim15.algoanalyst.*;
import apim15.algoanalyst.Result;
import javafx.embed.swing.JFXPanel;
import javafx.scene.Scene;

public class GraphDialog extends JFrame {
    private JToolBar toolbar;               
    private JFXPanel panelGraph;            
    private JComboBox GraphToChangeBox;     
    private JComboBox GraphToVisible;     
    private JCheckBox checkBoxLogarithmicAxes;
    private JComboBox TypeOfGraph;     
    private GraphArea graphScene;           
    private Result result;                  
    private JPanel panelG, panelT;          
    private HashMap mapTypeOfGraph, mapGraphByType;
    private JButton ZoomIn, ZoomOut, ZoomReset, upGraph, downGraph;
    private ArrayList<GraphDialogListener> listeners;
    private JScrollPane mainScrollPane;
    private JPanel mainPane;

    public static int height = 800, width = 1060;

    private final static String ZOOM_COMMAND = "ZOOM_COMMAND";
    private final static String ZOOM_COMMAND_OUT = "ZOOM_COMMAND_OUT";
    private final static String RESET_ZOOM_COMMAND = "RESET_ZOOM_COMMAND";
    private final static String UP_GRAPH_COMMAND = "UP_GRAPH_COMMAND";
    private final static String DOWN_GRAPH_COMMAND = "DOWN_GRAPH_COMMAND";
    private boolean isStartGraphArea,firstSelected=true, firstSelected2 = true;

    public GraphDialog(){
        super("Диаграмма");
        listeners = new ArrayList();
        mapTypeOfGraph = new HashMap();
        mapGraphByType = new HashMap();
        
        toolbar = new ToolBar() {
            
            @Override
            public void actionPerformed(ActionEvent e) {
                super.actionPerformed(e);
                int index;
                switch (e.getActionCommand()) {
                    case ZOOM_COMMAND:
                        graphScene.doZoom();
                        graphScene.resizeAxisY();
                        break;
                    case ZOOM_COMMAND_OUT:
                        graphScene.doZoomOut();
                        graphScene.resizeAxisY();
                        break;
                    case RESET_ZOOM_COMMAND:
                        graphScene.resetZoom();
                        graphScene.resizeAxisY();
                        break;
                    case DOWN_GRAPH_COMMAND:
                        index = GraphToChangeBox.getSelectedIndex();

                        if(index>=0 && index+1 < GraphToChangeBox.getItemCount()) {
                            graphScene.downGraph((int)mapGraphByType.get(GraphToChangeBox.getSelectedItem()));
                            GraphToChangeBox.insertItemAt(GraphToChangeBox.getSelectedItem(), index + 2);
                            GraphToChangeBox.removeItemAt(index);
                            GraphToChangeBox.setSelectedIndex(index + 1);
                        }
                        break;
                    case UP_GRAPH_COMMAND:
                        index = GraphToChangeBox.getSelectedIndex();

                        if(index>0) {
                            graphScene.upGraph((int)mapGraphByType.get(GraphToChangeBox.getSelectedItem()));
                            GraphToChangeBox.insertItemAt(GraphToChangeBox.getSelectedItem(), index - 1);
                            GraphToChangeBox.removeItemAt(index + 1);
                            GraphToChangeBox.setSelectedIndex(index - 1);
                        }
                        break;
                    default:
                        System.out.println("Unknown button");
                }
            }
        };
        
        ((ToolBar) toolbar).makeLabel("   Масштаб:   ");
        ZoomIn = ((ToolBar) toolbar).makeNavigationButton("+", ZOOM_COMMAND, "Zoom",
                "+");
        ZoomOut = ((ToolBar) toolbar).makeNavigationButton("-", ZOOM_COMMAND_OUT, "ZoomOut",
                "-");
        ZoomReset = ((ToolBar) toolbar).makeNavigationButton("R", RESET_ZOOM_COMMAND, "Reset",
                "R");

        checkBoxLogarithmicAxes = new JCheckBox("Lg");
        checkBoxLogarithmicAxes.setSelected(false);
        checkBoxLogarithmicAxes.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                graphScene.setLogarithmicAxesY();
            }
        });
        checkBoxLogarithmicAxes.setEnabled(false);
        toolbar.add(checkBoxLogarithmicAxes);

        ((ToolBar) toolbar).makeLabel("        Положение графика:  ");
        upGraph = ((ToolBar) toolbar).makeNavigationButton("up", UP_GRAPH_COMMAND, "Up to something-or-other",
                "Up");
        downGraph = ((ToolBar) toolbar).makeNavigationButton("down", DOWN_GRAPH_COMMAND, "Down to something-or-other",
                "Down");

        toolbar.setBorderPainted(true);
        toolbar.setEnabled(false);
        toolbar.setFloatable(false);
        toolbar.setRollover(true);
        toolbar.putClientProperty("JToolBar.isRollover", Boolean.TRUE);
        GraphToChangeBox = new JComboBox();
        GraphToChangeBox.setEnabled(false);
        toolbar.add(GraphToChangeBox);

        ((ToolBar) toolbar).makeLabel("        Отображение графиков:  ");
        GraphToVisible =  new MyJComboBox(){
            @Override
            public void actionPerformed(ActionEvent e) {
                if(firstSelected) {firstSelected=false; return;}
                if (getSelectedItem() instanceof JCheckBox) {
                    JCheckBox jcb = (JCheckBox)getSelectedItem();
                    jcb.setSelected(!jcb.isSelected());
                    if(result != null) {
                        if (jcb.isSelected()) {
                            addToGraphToChangeBox();
                                try {
                                    int index = (int)mapGraphByType.get(((JCheckBox) getSelectedItem()).getText());
                                    graphScene.addGraph(new Graph(result.results.elementAt(index),
                                            result.dimensions));
                                } catch (Exception ex) {
                                    ex.printStackTrace();
                                }
                        } else {
                            int index = removeFromGraphToChangeBox();
                            graphScene.removeGraph(index);
                        }
                    }
                }
            }
        };
        GraphToVisible.setEnabled(false);
        toolbar.add(GraphToVisible);

        ((ToolBar) toolbar).makeLabel("        Тип графика:  ");
        TypeOfGraph = new JComboBox();
        TypeOfGraph.setEnabled(false);
        TypeOfGraph.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                if(firstSelected2) {firstSelected2=false; return;}
                addGraphToAreaFromType((String)TypeOfGraph.getSelectedItem());
            }
        });
        toolbar.add(TypeOfGraph);

        panelGraph = new JFXPanel();
        graphScene = new GraphArea();
        panelG = new JPanel();
        panelT = new JPanel();
        panelT.add(toolbar);
        panelG.add(panelGraph);
        panelG.setBorder(new LineBorder(new Color(155, 100, 100)));
        mainPane = new JPanel();
        mainScrollPane = new JScrollPane(mainPane);
        mainScrollPane.setVerticalScrollBarPolicy(JScrollPane.VERTICAL_SCROLLBAR_NEVER);
        mainPane.setLayout(new GridBagLayout());
        GridBagConstraints c = new GridBagConstraints();
        c.gridy = 0;
        c.gridwidth = 4;
        c.gridx = 0;
        mainPane.add(panelT, c);
        c.gridwidth = 2;
        c.gridy = 1;
        c.gridx = 2;
        mainPane.add(panelG, c);
        mainPane.setBounds(400, 100, width, height);
        panelGraph.setPreferredSize(new Dimension(width - 50, height - 130));

        this.addComponentListener(new ComponentAdapter() {
            @Override
            public void componentResized(ComponentEvent e) {
                if(!isStartGraphArea) return;
                width = ((JFrame) e.getSource()).getWidth();
                height = ((JFrame) e.getSource()).getHeight();
                panelGraph.setPreferredSize(new Dimension(width - 50, height - 130));
                SwingUtilities.updateComponentTreeUI((JFrame) e.getSource());
 
            }
        });

        this.add(mainScrollPane);
        this.setMinimumSize(new Dimension(width,height));
        this.addWindowListener(new WindowAdapter() {
            @Override
            public void windowClosing(WindowEvent e) {
                super.windowClosing(e);
                for(GraphDialogListener ls: listeners)
                    ls.GraphDialogOnClose();
            }
        });
        setVisible(true);
    }

    public GraphDialog(Result R) {
        this();
        makeNewResult(R);
    }

    public void makeNewGraph(Detector detector) throws NullResultException
    {
        if(result==null) throw new NullResultException(this.getClass(), this.getClass().getEnclosingMethod());

        activateButtons();
        result.results.add(detector);
        result.protos.add(detector.getProto());

        if(!mapTypeOfGraph.containsValue(detector.getProto().getName())) {
            TypeOfGraph.addItem(detector.getProto().getName());
            mapTypeOfGraph.put(detector.getProto().getName().hashCode(),detector.getProto().getName());
        }

        if(((String)TypeOfGraph.getSelectedItem()).compareTo(detector.getProto().getName())==0) {
            mapGraphByType.put(detector.getName(), result.results.size()-1);
            JCheckBox checkButton = new JCheckBox(detector.getName());
            GraphToChangeBox.insertItemAt(detector.getName(), 0);
            GraphToVisible.addItem(checkButton);
            GraphToChangeBox.setSelectedIndex(0);
        }
    }

    public void makeNewResult(Result R) throws NullPointerException
    {
        Thread t = new Thread(new Runnable() {
            @Override
            public void run() {
                initFX();
            }
        });
        t.start();
        try {
            t.join();
            mapTypeOfGraph.clear();
            TypeOfGraph.removeAllItems();

            if (R != null) {
                if (R.dimensions == null)
                    throw new NullResultException(this.getClass(), this.getClass().getEnclosingMethod());
                result = new Result(R.protos);
                result.dimensions = new Vector<Integer>(R.dimensions);
                result.results = new Vector<Detector>(R.results);
                if (result.results.size() != 0) {
                    activateButtons();
                    Vector<Detector> res = result.results;

                    for (Detector D : res) {
                        if (!mapTypeOfGraph.containsValue(D.getProto().getName())) {
                            TypeOfGraph.addItem(D.getProto().getName());
                            mapTypeOfGraph.put(D.getProto().getName().hashCode(), D.getProto().getName());
                        }
                    }

                    addGraphToAreaFromType((String) TypeOfGraph.getSelectedItem());
                }
                this.setTitle(result.getName());
            }
            for (GraphDialogListener ls : listeners)
                ls.GraphDialogOnReady();
        } catch(Exception ex){ex.printStackTrace();}
    }

    private void addGraphToAreaFromType(String typeName)
    {
        graphScene.clear();
        GraphToChangeBox.removeAllItems();
        GraphToVisible.removeAllItems();
        mapGraphByType.clear();
        Vector<Detector> res = result.results;
       
        JCheckBox checkButton;
        int i = 0;
        firstSelected = true;
        for (Detector D : res) {
            if(typeName.compareTo(D.getProto().getName()) == 0) {
                mapGraphByType.put(D.getName(), i);
                checkButton = new JCheckBox(D.getName());
                GraphToChangeBox.insertItemAt(D.getName(), 0);
                checkButton.setSelected(true);
                GraphToVisible.addItem(checkButton);
                try {
                    graphScene.addGraph(new Graph(D, result.dimensions));
                } catch (Exception ex) {
                    ex.printStackTrace();
                }
            }
            i++;
        }
        GraphToChangeBox.setSelectedIndex(0);
    }

    private void activateButtons(){
        ZoomIn.setEnabled(true);
        ZoomOut.setEnabled(true);
        ZoomReset.setEnabled(true);
        upGraph.setEnabled(true);
        downGraph.setEnabled(true);
        GraphToChangeBox.setEnabled(true);
        GraphToVisible.setEnabled(true);
        TypeOfGraph.setEnabled(true);
        checkBoxLogarithmicAxes.setEnabled(true);
    }

    private void addToGraphToChangeBox()
    {
        GraphToChangeBox.insertItemAt(((JCheckBox)GraphToVisible.getSelectedItem()).getText(), 0);
    }

    private int removeFromGraphToChangeBox()
    {
        int count = GraphToChangeBox.getItemCount();
        String selectedString = ((JCheckBox)GraphToVisible.getSelectedItem()).getText();
        for(int i=0; i<count; i++ )
        {
            if (((String)GraphToChangeBox.getItemAt(i)).compareTo(selectedString)==0)
            {
                if(GraphToChangeBox.getSelectedIndex()==i)
                {
                    if(i>0)  GraphToChangeBox.setSelectedIndex(i-1);
                    else if(i+1<GraphToChangeBox.getItemCount())
                        GraphToChangeBox.setSelectedIndex(i+1);
                }
                GraphToChangeBox.removeItemAt(i);
                return i;
            }
        }
        return -1;
    }

    private void initFX() {
        // This method is invoked on the JavaFX thread
            Scene scene = graphScene.createScene();
            panelGraph.setScene(scene);
            isStartGraphArea = true;
    }


    public void addGraphDialogListener(GraphDialogListener wl)
    {
        listeners.add(wl);
    }

    public void removeGraphDialogOnCloseListener(int index)
    {
        listeners.remove(index);
    }

}