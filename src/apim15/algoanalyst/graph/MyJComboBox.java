package apim15.algoanalyst.graph;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class MyJComboBox extends JComboBox implements ActionListener {
    private RendererComboBox renderer;
   
    public MyJComboBox() {
        super();
        renderer = new RendererComboBox();
        setRenderer(renderer);
        addActionListener(this);
        setOpaque(true);
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        if (getSelectedItem() instanceof JCheckBox) {
            JCheckBox jcb = (JCheckBox)getSelectedItem();
            jcb.setSelected(!jcb.isSelected());
        }
    }
}
