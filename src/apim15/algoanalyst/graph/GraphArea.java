package apim15.algoanalyst.graph;

import javafx.application.Platform;
import javafx.beans.property.ObjectProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.collections.ObservableList;
import javafx.geometry.Point2D;
import javafx.scene.Node;
import javafx.scene.Scene;
import javafx.scene.chart.LineChart;
import javafx.scene.chart.NumberAxis;
import javafx.scene.chart.ValueAxis;
import javafx.scene.chart.XYChart;
import javafx.scene.control.Tooltip;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.StackPane;
import javafx.scene.paint.Color;
import javafx.scene.shape.Rectangle;
import javafx.util.StringConverter;

import java.util.ArrayList;
import java.util.Stack;
import java.util.Vector;

public class GraphArea {

    private volatile ArrayList<Graph> Graphs;
    private Rectangle zoomRect;
    private LineChart<Number, Number> chart;
    private StackPane chartContainer;
    private Stack<Double> upperBoundsX, upperBoundsY;

    private double maxX, minX;
    private double maxY = Double.MIN_VALUE, minY = Double.MAX_VALUE;
    private volatile boolean isLogarithmicAxes = false;
    private NumberAxis axisX;

    public GraphArea() {
        Graphs = new ArrayList<Graph>();
        upperBoundsX = new Stack();
        upperBoundsY = new Stack();
    }

    public void addGraph(Graph g) {
        Platform.runLater(() -> {
            if (chart == null) {
                chart = createChart();
            }
            addGraphToChart(g);
            validateGraphForAxisYLimit(g);

            Graphs.add(g);
            resetAxisX();
            resetAxisY();
        });
    }

    private void addGraphToChart(Graph g) {
        if(!isLogarithmicAxes) {
            chart.getData().add(g.createSeries());
        }
        else {
            chart.getData().add(g.createSeries());
        }
        ObservableList<XYChart.Data> dataList = 
                ((XYChart.Series) chart.getData().get(chart.getData().size() - 1)).getData();

        dataList.forEach(data -> {
            Node node = data.getNode();
            String y = ""; int pow;
            
            for(int i = 1; i < 6; i++) {
                pow = (int)Math.pow(10.0,(double)i);
                if(g.hasDouble) {
                    Double cmp = (Double) data.getYValue() * pow % pow;
                    if (cmp > 0) {
                        y = String.format("%1$." + i + "f", data.getYValue());
                        break;
                    }
                } else {
                    Integer cmp = (Integer)data.getYValue() * pow % pow;
                    if (cmp > 0) {
                        y = String.valueOf(data.getYValue());
                        break;
                    }
                }
            }
            if(g.hasDouble) {
                if (y == "") y = String.format("%1$.0f", data.getYValue());
            } 
            else {
                y = String.valueOf(data.getYValue());
            }
            
            Tooltip tooltip = new Tooltip('(' + data.getXValue().toString()
                   + "; " + y + ')');
            Tooltip.install(node, tooltip);
        });
    }


    private void validateGraphForAxisYLimit(Graph g)
    {
            if (maxY < g.maxY) maxY = g.maxY;
            if (minY > g.minY) minY = g.minY;
    }

    private void resetAxisY()
    {
        if(!isLogarithmicAxes) {
            final NumberAxis yAxis = (NumberAxis) chart.getYAxis();
            if(Graphs.size()!=0) {
                yAxis.setLabel(Graphs.get(0).Ylabel);
            }
            yAxis.setLowerBound(minY-((maxY-minY)*0.05));
            yAxis.setUpperBound(maxY+((maxY-minY)*0.05));
            resizeAxisY();
        } 
        else {
            final LogarithmicAxis yAxis = (LogarithmicAxis) chart.getYAxis();
            Number []Range = new Number[2];
            Range[0] = minY;
            Range[1] = maxY+((maxY-minY)*0.05);
            yAxis.setRange(Range,false);
            if (Graphs.size() != 0) {
                yAxis.setLabel(Graphs.get(0).Ylabel);
            }
        }
    }

    /**
     * Recalculation axis 
     */
    public void resizeAxisY(){
        double YuB = ((ValueAxis) chart.getYAxis()).getUpperBound();
        double YlB = ((ValueAxis) chart.getYAxis()).getLowerBound();

        if(!isLogarithmicAxes){
            ((NumberAxis)chart.getYAxis()).setTickUnit((YuB - YlB)/10.0);
        }
    }

    /** 
     * Remove graphic with index
     */
    public void removeGraph(int index)
    {
        Platform.runLater(() -> {
            int ind = Graphs.size() - index - 1;
            Graph g = Graphs.get(ind);
            Graphs.remove(g);
            if(!isLogarithmicAxes) {
                if (maxY == g.maxY || minY == g.minY) {
                    updateGraphMaxMin();
                    resetAxisY();
                }
            }
            else {
                if(maxY == g.maxYLog || minY == g.minYLog)
                {
                    updateGraphMaxMin();
                    resetAxisY();
                }
            }
            chart.getData().remove(ind);
        });
    }

    /** 
     * Clear graph area
     */
    public void clear()
    {
        Platform.runLater(() -> {
            if(chart!=null) {
               chart.getData().clear();
            }
            if(Graphs!=null) {
                Graphs.clear();
            }
            updateGraphMaxMin();
        });
    }

    /**
     * Move graph down
     */
    public void downGraph(int index)
    {
        Platform.runLater(new Runnable() {
            @Override
            public void run() {
                    Graph g = Graphs.remove(index);
                    Graphs.add(index-1,g);
                    XYChart.Series s = chart.getData().remove(index-1);
                    chart.getData().add(index, s);
            }
        });

    }

    /**
     * Move graph up
     */
    public void upGraph(int index)
    {
        Platform.runLater(new Runnable() {
            @Override
            public void run() {
                int ind = Graphs.size() - index - 1;
                Graph g = Graphs.remove(ind);
                Graphs.add(ind+1,g);
                XYChart.Series s = chart.getData().remove(ind);
                chart.getData().add(ind+1, s);
            }
        });

    }

    /**
     * Need for update axis with new graph
     */
    private void updateGraphMaxMin()
    {
        maxY=Integer.MIN_VALUE;
        minY =Integer.MAX_VALUE;
        for (Graph g : Graphs) {
            validateGraphForAxisYLimit(g);
        }
    }

    private LineChart<Number,Number> createChart() {

        if(axisX == null) {
            axisX = createAxis();
        }
        LineChart<Number, Number> numberLineChart;
        ValueAxis y,x;
        if(isLogarithmicAxes) {
            y = createLogAxis();
            x = createLogAxis();
        } 
        else {
            y  = createAxis();
            x = axisX;
        }
        numberLineChart = new LineChart<Number, Number>(x,y);
        numberLineChart.setAxisSortingPolicy(LineChart.SortingPolicy.NONE);
        numberLineChart.setAnimated(false);
        numberLineChart.setCreateSymbols(true);
        return numberLineChart;
    }

    /**
     * Create fiagram scene
     */
    public Scene createScene() {
        if(chart == null) {
            chart = createChart();
        }
        chartContainer = new StackPane();
        chartContainer.getChildren().add(chart);

        zoomRect = new Rectangle();
        zoomRect.setManaged(false);
        zoomRect.setFill(Color.LIGHTSEAGREEN.deriveColor(1, 1, 1, 0.2));
        chartContainer.getChildren().add(zoomRect);
        setUpZooming(zoomRect, chart);

        final BorderPane root = new BorderPane();
        root.setCenter(chartContainer);

        final Scene scene = new Scene(root, 600, 400);
        return scene;
    }

    private NumberAxis createAxis() {
        final NumberAxis Axis = new NumberAxis();
        Axis.setAutoRanging(false);
        return Axis;
    }

    private LogarithmicAxis createLogAxis() {
        final LogarithmicAxis Axis = new LogarithmicAxis();
        Axis.setAutoRanging(false);
        return Axis;
    }

    private void resetAxisX() {
        if(isLogarithmicAxes) {
            final LogarithmicAxis xAxis = (LogarithmicAxis) chart.getXAxis();
            Number[] Range = new Number[2];
            if(minX > 0) {
                Range[0] = minX;
            }
            else {
                Range[0] = minX+((maxX - minX) * 0.05);
            }
            Range[1] = maxX + ((maxX - minX) * 0.05);
            xAxis.setRange(Range, false);
            if (Graphs.size() != 0) {
                xAxis.setLabel(Graphs.get(0).Xlabel);
            }
            return;
        }

        if(Graphs.size()==0 || Graphs.size()>1) {
            return;
        }
        Vector<Integer> dimensions = Graphs.get(0).X;
        if(dimensions.size() == 0) {
            return;
        }
        final NumberAxis Axis = (NumberAxis)chart.getXAxis();
        Axis.setLabel(Graphs.get(0).Xlabel);
        minX = dimensions.elementAt(0);
        maxX = dimensions.elementAt(dimensions.size()-1);

        Axis.setTickLabelFormatter(new StringConverter<Number>() {
            @Override
            public String toString(Number object) {
                for(Integer i: dimensions)
                {
                    if(object.doubleValue()==i.doubleValue()) {
                            return Integer.toString(i.intValue());
                    }
                }
                return null;
            }

            @Override
            public Number fromString(String string) {
                return null;
            }
        });

        int sizeD = dimensions.size();
        Vector<Integer> Gaps = new Vector();
        for(int i = sizeD - 1; i > 0; i--)
        {
            Gaps.add(dimensions.elementAt(i)-dimensions.elementAt(i-1));
        }
        Gaps.sort(Integer::compareTo);
        int gcd = gcd_n(Gaps);
        Axis.setTickUnit((double)gcd);
        Axis.setMinorTickVisible(false);
        Axis.setTickMarkVisible(true);
        Axis.setLowerBound(minX);
        Axis.setUpperBound(maxX);

    }

    private void setUpZooming(final Rectangle rect, final Node zoomingNode) {
        final ObjectProperty<Point2D> mouseAnchor = new SimpleObjectProperty<>();
        zoomingNode.setOnMousePressed(event -> {
            mouseAnchor.set(new Point2D(event.getX(), event.getY()));
            rect.setWidth(0);
            rect.setHeight(0);
        });
        zoomingNode.setOnMouseDragged(event -> {
            double x = event.getX();
            double y = event.getY();
            rect.setX(Math.min(x, mouseAnchor.get().getX()));
            rect.setY(Math.min(y, mouseAnchor.get().getY()));
            rect.setWidth(Math.abs(x - mouseAnchor.get().getX()));
            rect.setHeight(Math.abs(y - mouseAnchor.get().getY()));
        });
    }

    /**
     * Zoom in
     */
    public void doZoom() {
        if(Graphs.size() == 0) return;
        Point2D zoomTopLeft,zoomBottomRight;
        if(zoomRect.getWidth() != 0 && zoomRect.getHeight() !=0) {
            zoomTopLeft = new Point2D(zoomRect.getX(), zoomRect.getY());
            zoomBottomRight = new Point2D(zoomRect.getX() + zoomRect.getWidth(), 
                    zoomRect.getY() + zoomRect.getHeight());
        }
        else {
            zoomTopLeft = new Point2D(16, 200);
            zoomBottomRight = new Point2D(chartContainer.getWidth()-100, chartContainer.getHeight()-112);
        }
        final ValueAxis yAxis = (ValueAxis) chart.getYAxis();
        Point2D yAxisInScene = yAxis.localToScene(0, 0);
        final ValueAxis xAxis = (ValueAxis) chart.getXAxis();
        Point2D xAxisInScene = xAxis.localToScene(0, 0);
        double xOffset = 0;
        if(zoomRect.getWidth() != 0 && zoomRect.getHeight() !=0) {
            xOffset = zoomTopLeft.getX() - xAxisInScene.getX();
        }
        else {
            xOffset = zoomTopLeft.getX() - yAxisInScene.getX();
        }
        double yOffset = zoomBottomRight.getY() - xAxisInScene.getY();
        double xAxisScale = xAxis.getScale();
        double yAxisScale = yAxis.getScale();
        xAxis.setLowerBound(xAxis.getLowerBound() + xOffset / xAxisScale);
        yAxis.setLowerBound( yAxis.getLowerBound() + yOffset / yAxisScale);
        if(zoomRect.getWidth() != 0 && zoomRect.getHeight() != 0) {
            xAxis.setUpperBound(xAxis.getLowerBound() + zoomRect.getWidth() / xAxisScale);
            yAxis.setUpperBound(yAxis.getLowerBound() - zoomRect.getHeight() / yAxisScale);
        } else {
            upperBoundsX.push(xAxis.getUpperBound());
            upperBoundsY.push(yAxis.getUpperBound());
            xAxis.setUpperBound(xAxis.getLowerBound() + (chartContainer.getWidth()-180) / xAxisScale);
            yAxis.setUpperBound(yAxis.getLowerBound() - (chartContainer.getHeight()-180) / yAxisScale);
        }
        zoomRect.setWidth(0);
        zoomRect.setHeight(0);
    }

    /**
     * Zoom out
     */
    public void doZoomOut() {
        if(Graphs.size() == 0) return;
        if(upperBoundsX.size()==0)return;
        Point2D zoomTopLeft,zoomBottomRight;
            zoomTopLeft = new Point2D(16, 200);
            zoomBottomRight = new Point2D(chartContainer.getWidth()-100,
                    chartContainer.getHeight()-112);

        final ValueAxis yAxis = (ValueAxis) chart.getYAxis();
        Point2D yAxisInScene = yAxis.localToScene(0, 0);
        final ValueAxis xAxis = (ValueAxis) chart.getXAxis();
        Point2D xAxisInScene = xAxis.localToScene(0, 0);
        double xOffset = zoomTopLeft.getX() - yAxisInScene.getX() ;
        double yOffset = zoomBottomRight.getY() - xAxisInScene.getY();
        double xAxisScale = xAxis.getScale();
        double yAxisScale = yAxis.getScale();
        xAxis.setLowerBound(xAxis.getLowerBound() + xOffset / xAxisScale);
        yAxis.setLowerBound(yAxis.getLowerBound() + yOffset / yAxisScale);
            xAxis.setUpperBound(upperBoundsX.pop());
            yAxis.setUpperBound(upperBoundsY.pop());
    }

    /**
     * Default zoom
     */
    public void resetZoom(){
        if(Graphs.size() == 0) return;
        final ValueAxis xAxis = (ValueAxis)chart.getXAxis();
        if(!isLogarithmicAxes) {
            xAxis.setLowerBound(minX);
            xAxis.setUpperBound(maxX);
        } 
        else {
            resetAxisX();
        }
        
        final ValueAxis yAxis = (ValueAxis)chart.getYAxis();
        if(!isLogarithmicAxes) {
            yAxis.setLowerBound(minY - ((maxY - minY) * 0.05));
            yAxis.setUpperBound(maxY + ((maxY - minY) * 0.05));
        } 
        else {
            Number []Range = new Number[2];
            Range[0] = minY;
            Range[1] = maxY+((maxY-minY)*0.05);
            ((LogarithmicAxis)yAxis).setRange(Range,false);
        }
        
        zoomRect.setWidth(0);
        zoomRect.setHeight(0);
        upperBoundsX.clear();
        upperBoundsY.clear();
    }

    private int gcd_n(Vector<Integer> V)
    {
        int result=Integer.MAX_VALUE;
        int c;
        for(int i = 0; i < V.size(); i++)
            for(int j = i + 1; j < V.size(); j++)
                if((c = gcd_1(V.get(i), V.get(j))) < result){
                    result = c;
                }
        if(result == 0) {
            result = 1;
        }
        
        return result;
    }

    private int gcd_1(int a, int b) {
        if (b == 0) {
            return a;
        }
        return gcd_1(b, a % b);
    }

    /**
     * Change axis type to logarithmic
     */
    public void setLogarithmicAxesY()
    {
        Platform.runLater(()-> {
            isLogarithmicAxes = !isLogarithmicAxes;
            chartContainer.getChildren().clear();
            chart = createChart();
            setUpZooming(zoomRect, chart);
            chartContainer.getChildren().add(chart);
            chartContainer.getChildren().add(zoomRect);
            for (Graph g : Graphs) {
                addGraphToChart(g);
            }

            resetAxisY();
            resetAxisX();
        });
    }
    
}
