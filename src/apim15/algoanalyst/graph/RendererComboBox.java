package apim15.algoanalyst.graph;

import javax.swing.*;
import java.awt.*;
import java.util.Vector;

public class RendererComboBox implements ListCellRenderer {

    private Vector<JCheckBox> checkBoxes;
    private JLabel defaultLabel;

    public RendererComboBox()
    {
        checkBoxes = new Vector();
    }

    @Override
    public Component getListCellRendererComponent(JList list, Object value, 
            int index, boolean isSelected, boolean cellHasFocus) {

        if(value == null) {
            value = "";
        }
        if (value instanceof Component) {
            JCheckBox c = (JCheckBox)value;
            Color background = Color.WHITE;
            Color foreground = Color.BLACK;
            JList.DropLocation dropLocation = list.getDropLocation();
            if (dropLocation != null && 
                    !dropLocation.isInsert() &&
                    dropLocation.getIndex() == index) {

                background = Color.BLUE;
                foreground = Color.WHITE;

            } 
            else if (isSelected) {
                background = Color.RED;
                foreground = Color.WHITE;
            }
            c.setBackground(background);
            c.setForeground(foreground);
            
            return c;
        } 
        else {
            if (defaultLabel == null) {
                defaultLabel = new JLabel(value.toString());
            }
            else {
                defaultLabel.setText(value.toString());
            }
            
            return defaultLabel;
        }
    }
}

