package apim15.algoanalyst.graph;

import javax.swing.*;
import javax.swing.border.Border;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.net.URL;

public class ToolBar extends JToolBar implements ActionListener{

    public ToolBar() {
        super();
        this.setBackground(new Color(200,200,200));
    }

    public JButton makeNavigationButton(String imageName,
                                           String actionCommand,
                                           String toolTipText,
                                           String altText) {

        String imgLocation = "images/" + imageName + ".png";
        
        URL imageURL = ToolBar.class.getResource(imgLocation);
        
        JButton button = new JButton();
        button.setActionCommand(actionCommand);
        button.setToolTipText(toolTipText);
        button.addActionListener(this);
        button.setBorder(BorderFactory.createEmptyBorder(10,10,10,10));
        button.setEnabled(false);

        if (imageURL != null) {                      
            button.setIcon(new ImageIcon(imageURL, altText));
        } 
        else {                                     
            button.setText(altText);
            System.err.println("Resource not found: " + imgLocation);
        }

        this.add(button);

        return button;
    }

    public void makeLabel(String message)
    {
        JLabel label = new JLabel(message);
        this.add(label);
    }

    @Override
    public void actionPerformed(ActionEvent e) {

    }
}
