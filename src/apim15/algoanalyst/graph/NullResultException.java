package apim15.algoanalyst.graph;

import java.lang.reflect.Method;

public class NullResultException extends NullPointerException{
        public Class senderClass;
        public Method senderMethod;
        
        public NullResultException(Class senderClass, Method senderMethod) {
            super(senderClass.getName() + "." + senderMethod.getName() + 
                    ": Не инициализирован объект с данными!");
            this.senderClass = senderClass;
            this.senderMethod = senderMethod;
        }

        public void printClassAndMethod() {
            System.out.println(senderClass.getName() + "." + senderMethod.getName());
        }
}
