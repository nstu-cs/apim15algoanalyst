package apim15.algoanalyst.graph;

import apim15.algoanalyst.Detector;
import apim15.algoanalyst.I_DetectorValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.scene.chart.XYChart;
import java.util.Vector;


public class Graph {

    public Vector<Integer> X;
    public Vector<I_DetectorValue> Y;
    public String Name, Xlabel, Ylabel;
    public double maxY, minY, maxYLog, minYLog;
    public boolean hasDouble;

    public Graph(Detector d, Vector<Integer> dimension) throws Exception {
        Y = new Vector(d.getData());
        if(dimension == null) {
            throw new Exception("Bad dimension");
        }
        X = new Vector(dimension);
        bubbleSort();

        maxY = Double.MIN_VALUE;
        minY = Double.MAX_VALUE;
        hasDouble = d.getProto().hasDouble();
        for(I_DetectorValue dValue: Y) {
            if(hasDouble) {
                if (maxY < dValue.getDouble()) maxY = dValue.getDouble();
                if (minY > dValue.getDouble()) minY = dValue.getDouble();
            } 
            else {
                if (maxY < dValue.getDouble()) maxY = dValue.getInt();
                if (minY > dValue.getDouble()) minY = dValue.getInt();
            }
        }
        maxYLog = Math.log10(maxY);
        minYLog = Math.log10(minY);
        Name = d.getName();
        Ylabel = d.getProto().getName();
        Xlabel = "Count of step";
    }

    public XYChart.Series createSeries() {
        XYChart.Series series1 = new XYChart.Series();
        series1.setName(Name);
        ObservableList<XYChart.Data> datas = FXCollections.observableArrayList();

        for(int i=0; i<X.size(); i++){
            if(hasDouble) {
                double el = 1.0;         
                if(Y.elementAt(i).getDouble() > 0.0) {
                    el = Y.elementAt(i).getDouble();
                }
                datas.add(new XYChart.Data(X.elementAt(i), el));
            } 
            else {
                int el = 1;
                if(Y.elementAt(i).getInt() > 0) {
                    el = Y.elementAt(i).getInt();
                }
                datas.add(new XYChart.Data(X.elementAt(i), el));
            }
        }
        series1.setData(datas);
        return series1;
    }

    public void bubbleSort(){
        for(int i = X.size()-1 ; i > 0 ; i--) {
            for(int j = 0 ; j < i ; j++){
                if( X.get(j) > X.get(j+1) ){
                    int tmp = X.get(j);
                    X.set(j,X.get(j+1));
                    X.set(j+1, tmp);

                    I_DetectorValue tmp2 = Y.get(j);
                    Y.set(j,Y.get(j+1));
                    Y.set(j+1, tmp2);
                }
            }
        }
    }

}
