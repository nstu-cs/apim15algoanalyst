package apim15.algoanalyst.graph;

public interface GraphDialogListener {
    void GraphDialogOnClose();
    void GraphDialogOnReady();
}
