/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package apim15.algoanalyst;

import javax.swing.JComboBox;

/**
 *
 * @author romanow
 */
public class BoxFactory<T extends I_Named>{
    public interface BoxFactoryCallBack<T>{
        public void getSelected(T selectedItem);
        }
    private I_NamedFactory<T> factory;
    private JComboBox box;
    public T getSelected(){
        return factory.getByName(box.getSelectedItem().toString());
        }
    public BoxFactory(I_NamedFactory<T> factory,JComboBox box,final BoxFactoryCallBack<T> back){
        this.box = box;
        this.factory = factory;
        box.removeAllItems();
        String names[] = factory.createList();
        box.addItem("...");
        for(int i=0;i<names.length;i++)
            box.addItem(names[i]);
        final I_NamedFactory<T> factory1 = factory;
        box.addItemListener(new java.awt.event.ItemListener() {
            public void itemStateChanged(java.awt.event.ItemEvent evt) {
                String ss = (String)evt.getItem();
                T vv = factory1.getByName(ss);
                if (vv==null)
                    return;
                back.getSelected(vv);
                }
            });
        }
    public void setSelectedName(String name){
        box.setSelectedItem(name);
        }
    public String getSelectedName(){
        return (String)box.getSelectedItem();
        }
}
