package apim15.algoanalyst.test;

import apim15.algoanalyst.I_File;
import apim15.algoanalyst.data.DataFactory;
import apim15.algoanalyst.data.I_DataElement;
import apim15.algoanalyst.data.I_SequenceGenerator;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author eremin
 */
public class TestCase implements I_File {
    private ArrayList <I_DataElement> testData = new ArrayList<>();

    // for serialization purposes: instantiate using this, then call load()
    public TestCase() {}

    public TestCase(int size, I_SequenceGenerator gen) {
        testData.ensureCapacity(size);
        gen.reset();

        for(int i = 0; i < size; ++i)
            testData.add(gen.getNext());
    }

    public List<I_DataElement> getTestData() {
        return testData;
    }

    @Override
    public void save(DataOutputStream out) throws IOException {
        out.writeInt(testData.size());
        if(testData.isEmpty())
            return;

        out.writeUTF(testData.get(0).getName());
        for(I_DataElement e : testData)
            out.writeUTF(e.toString());
    }

    @Override
    public void load(DataInputStream in) throws IOException {
        int size = in.readInt();
        testData.clear();

        if(size == 0)
            return;

        testData.ensureCapacity(size);

        String typeName = in.readUTF();
        I_DataElement e = new DataFactory().getByName(typeName);
        for(int i = 0; i < size; ++i) {
            e = e.clone();
            e.parse(in.readUTF());
            testData.add(e);
        }
    }
}
