package apim15.algoanalyst.test;

import apim15.algoanalyst.conf.IllegalParameterException;
import apim15.algoanalyst.conf.ParameterFormatException;
import apim15.algoanalyst.conf.ParameterSet;
import apim15.algoanalyst.conf.ParameterSet.Parameter;

import java.util.List;

/**
 * Generates a list of dimensions that are equal to a specified constant.
 * @author eremin
 */
public class ConstantDimensionGenerator extends DimensionGenerator {
    private static final String PARAM_SIZE = "Size";

    private int constantSize = 16;

    @Override
    public ParameterSet getParams() {
        ParameterSet params = new ParameterSet();
        params.add(new Parameter(PARAM_SIZE, "Size of all test cases", Integer.toString(constantSize)));
        return params;
    }

    @Override
    public void setParams(ParameterSet params) throws ParameterFormatException, IllegalParameterException {
        for(Parameter p : params.getParameters())
            if(p.getName().equals(PARAM_SIZE)) {
                constantSize = parseIntOrThrow(p.getValue(), PARAM_SIZE, "Size must be an integer number");
                break;
            }

        if(constantSize < 0) {
            constantSize = 16;
            throw new IllegalParameterException("Size of a test case cannot be negative", PARAM_SIZE);
        }
    }

    @Override
    public String getName() {
        return ConstantDimensionGenerator.class.getSimpleName();
    }

    @Override
    protected List<Integer> generateImpl(int size, List<Integer> list) {
        for(int i = 0; i < size; ++i)
            list.add(constantSize);

        return list;
    }
}
