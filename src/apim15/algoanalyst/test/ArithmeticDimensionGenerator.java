package apim15.algoanalyst.test;

import apim15.algoanalyst.conf.IllegalParameterException;
import apim15.algoanalyst.conf.ParameterFormatException;
import apim15.algoanalyst.conf.ParameterSet;
import apim15.algoanalyst.conf.ParameterSet.Parameter;

import java.util.List;

/**
 * Generates arithmetic sequence of sizes.
 * @author eremin
 */
public class ArithmeticDimensionGenerator extends DimensionGenerator {
    private static final String PARAM_INITIAL = "Initial term";
    private static final String PARAM_DIFFERENCE = "Difference";

    private int initialTerm = 1, difference = 1;

    @Override
    public ParameterSet getParams() {
        Parameter initial = new Parameter(PARAM_INITIAL,
                "Initial term of an arithmetic sequence",
                Integer.toString(initialTerm));

        Parameter diff = new Parameter(PARAM_DIFFERENCE,
                "Difference of an arithmetic sequence",
                Integer.toString(difference));

        return new ParameterSet()
                .add(initial)
                .add(diff);
    }

    @Override
    public void setParams(ParameterSet params) throws ParameterFormatException, IllegalParameterException {
        for(Parameter p : params.getParameters())
            if(p.getName().equals(PARAM_INITIAL))
                initialTerm = parseIntOrThrow(p.getValue(),
                        PARAM_INITIAL,
                        "Initial term of a sequence must be an integer");
            else if(p.getName().equals(PARAM_DIFFERENCE))
                difference = parseIntOrThrow(p.getValue(),
                        PARAM_DIFFERENCE,
                        "Difference must be an integer");

        if(initialTerm < 0) {
            initialTerm = 1;
            throw new IllegalParameterException("Initial term of an arithmetic sequence cannot be negative",
                    PARAM_INITIAL);
        }
    }

    @Override
    public String getName() {
        return ArithmeticDimensionGenerator.class.getSimpleName();
    }

    @Override
    protected List<Integer> generateImpl(int size, List<Integer> list) {
        /*
            If difference is negative we have a diminishing arithmetic sequence.
            It's okay to have diminishing sequence, but it might start generating not positive
            numbers withing the range of size argument. Good implementation must somehow
            notify user that such a situation occured, but we're lazy enough and it's just
            a prototype, so we're just filling the list with zeroes silently.
         */

        for(int i = 0; i < size; ++i) {
            int current = initialTerm + i * difference;
            list.add(current >= 0 ? current : 0);
        }

        return list;
    }
}
