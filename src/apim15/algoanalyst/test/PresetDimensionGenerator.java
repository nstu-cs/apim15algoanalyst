package apim15.algoanalyst.test;

import apim15.algoanalyst.conf.IllegalParameterException;
import apim15.algoanalyst.conf.ParameterFormatException;
import apim15.algoanalyst.conf.ParameterSet;
import apim15.algoanalyst.conf.ParameterSet.Parameter;
import java.util.ArrayList;

import java.util.List;

/**
 * Generates a list of dimensions by repeating supplied list.
 * @author eremin
 */
public class PresetDimensionGenerator extends DimensionGenerator {
    private static final String PARAM_SIZE_LIST = "Size list";

    private ArrayList<Integer> sizeList = new ArrayList<>();

    public PresetDimensionGenerator() {
        sizeList.add(16);
        sizeList.add(32);
        sizeList.add(64);
    }

    @Override
    public ParameterSet getParams() {
        StringBuilder value = new StringBuilder();
        for(int i : sizeList)
            value.append(i).append(", ");

        value.delete(value.length() - 2, value.length());

        ParameterSet params = new ParameterSet();
        params.add(new Parameter(PARAM_SIZE_LIST, "Comma-separated list of sizes", value.toString()));
        return params;
    }

    @Override
    public void setParams(ParameterSet params) throws ParameterFormatException, IllegalParameterException {
        String[] value = {""};
        for(Parameter p : params.getParameters())
            if(p.getName().equals(PARAM_SIZE_LIST))
                value = p.getValue().split(", ");

        ArrayList<Integer> newList = new ArrayList<>(value.length);
        for(String s : value) {
            int size = parseIntOrThrow(s, PARAM_SIZE_LIST, "Size must be an integer: " + s);
            if(size < 0)
                throw new IllegalParameterException("Size of a test case cannot be negative: " + s, PARAM_SIZE_LIST);

            newList.add(size);
        }

        sizeList = newList;
    }

    @Override
    public String getName() {
        return PresetDimensionGenerator.class.getSimpleName();
    }

    @Override
    protected List<Integer> generateImpl(int size, List<Integer> list) {
        int idx = 0;
        for(int i = 0; i < size; ++i) {
            list.add(sizeList.get(idx));
            idx = (idx + 1) % sizeList.size();
        }

        return list;
    }
}
