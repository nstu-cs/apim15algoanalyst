package apim15.algoanalyst.test;

import apim15.algoanalyst.I_Named;
import apim15.algoanalyst.conf.I_Configurable;
import apim15.algoanalyst.conf.IllegalParameterException;
import apim15.algoanalyst.conf.ParameterFormatException;
import apim15.algoanalyst.conf.ParameterSet;
import java.util.ArrayList;

import java.util.List;

/**
 * @author eremin
 */
public abstract class DimensionGenerator implements I_Configurable, I_Named, I_DimensionGenerator {

    public List<Integer> generate(int size) throws IllegalArgumentException {
        if(size < 0)
            throw new IllegalArgumentException("Size of a test plan cannot be negative");

        return generateImpl(size, new ArrayList<Integer>(size));
    }

    @Override
    public abstract ParameterSet getParams();

    @Override
    public abstract void setParams(ParameterSet params) throws ParameterFormatException, IllegalParameterException;

    @Override
    public String getName() {
        return DimensionGenerator.class.getSimpleName();
    }

    protected abstract List<Integer> generateImpl(int size, List<Integer> list);

    protected int parseIntOrThrow(String number, String paramName, String errorMsg) throws ParameterFormatException {
        try {
            return Integer.parseInt(number);
        } catch(NumberFormatException e) {
            throw new ParameterFormatException(errorMsg, paramName);
        }
    }
}
