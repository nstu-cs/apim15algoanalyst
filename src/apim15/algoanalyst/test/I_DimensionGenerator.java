package apim15.algoanalyst.test;

import apim15.algoanalyst.I_Named;
import java.util.List;

/**
 *
 * @author User
 */
public interface I_DimensionGenerator extends I_Named {
    public List<Integer> generate(int size) throws IllegalArgumentException;
}
