package apim15.algoanalyst.test;

import apim15.algoanalyst.conf.IllegalParameterException;
import apim15.algoanalyst.conf.ParameterFormatException;
import apim15.algoanalyst.conf.ParameterSet;
import apim15.algoanalyst.conf.ParameterSet.Parameter;

import java.util.List;

/**
 * Generates geometric sequence of sizes.
 * @author eremin
 */
public class GeometricDimensionGenerator extends DimensionGenerator {
    private static final String PARAM_INITIAL = "Initial term";
    private static final String PARAM_RATIO = "Common ratio";

    private int initialTerm = 2, commonRatio = 2;

    @Override
    public ParameterSet getParams() {
        Parameter initial = new Parameter(PARAM_INITIAL,
                "Initial term of a geometric sequence",
                Integer.toString(initialTerm));

        Parameter ratio = new Parameter(PARAM_RATIO,
                "Common ratio of a geometric sequence",
                Integer.toString(commonRatio));

        return new ParameterSet()
                .add(initial)
                .add(ratio);
    }

    @Override
    public void setParams(ParameterSet params) throws ParameterFormatException, IllegalParameterException {
        for(Parameter p : params.getParameters())
            if(p.getName().equals(PARAM_INITIAL))
                initialTerm = parseIntOrThrow(p.getValue(), PARAM_INITIAL, "Initial term must be an integer");
            else if(p.getName().equals(PARAM_RATIO))
                commonRatio = parseIntOrThrow(p.getValue(), PARAM_RATIO, "Common ratio must be an integer");

        if(initialTerm < 0) {
            initialTerm = 2;
            throw new IllegalParameterException("Initial term must be positive", PARAM_INITIAL);
        }

        if(commonRatio < 0) {
            commonRatio = 2;
            throw new IllegalParameterException("Common ratio must be positive", PARAM_RATIO);
        }
    }

    @Override
    public String getName() {
        return GeometricDimensionGenerator.class.getSimpleName();
    }

    @Override
    protected List<Integer> generateImpl(int size, List<Integer> list) {
        int current = initialTerm;
        for(int i = 0; i < size; ++i) {
            list.add(current);
            current *= commonRatio;
        }
        return list;
    }
}
