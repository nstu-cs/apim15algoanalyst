package apim15.algoanalyst.test;

import apim15.algoanalyst.I_File;
import apim15.algoanalyst.I_Named;
import apim15.algoanalyst.data.I_SequenceGenerator;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

/**
 * @author eremin
 */
public class TestPlan implements I_Named, I_File {

    private ArrayList<TestCase> testCases = new ArrayList<>();
    private String name = "";

    // for serialization purposes: instantiate using this, then call load()
    public TestPlan() {}

    public TestPlan(String name, int size, I_SequenceGenerator sequenceGen, I_DimensionGenerator dimenGen) {
        testCases.ensureCapacity(size);
        this.name = name;

        for(int len : dimenGen.generate(size))
            testCases.add(new TestCase(len, sequenceGen));
    }

    public List<TestCase> getTestCases() {
        return testCases;
    }

    @Override
    public void save(DataOutputStream out) throws IOException {
        out.writeUTF(getName());
        out.writeInt(testCases.size());

        for(TestCase t : testCases)
            t.save(out);
    }

    @Override
    public void load(DataInputStream in) throws IOException {
        name = in.readUTF(); // unused now
        int size = in.readInt();
        testCases.clear();

        if(size == 0)
            return;

        for(int i = 0; i < size; ++i) {
            TestCase t = new TestCase();
            t.load(in);
            testCases.add(t);
        }
    }

    @Override
    public String getName() {
        return name;
    }
}
