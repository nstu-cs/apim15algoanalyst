package apim15.algoanalyst.data;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.util.Random;

public class StringElement extends DataElement {
    private String value;

    public StringElement(String value) {
        this.value = value;
    }
    
    public StringElement() {
        value = "";
    }
    
    private String generateRandomString(int length) {
        String chars = new String("qwertyuiopasdfghjklzxcvbnmQWERTYUIOPASDFGHJKLZXCVBNM1234567890");
        String result = "";
        Random rand = new Random();
        for (int i = 0; i < length; i++){
            result = result + chars.charAt(rand.nextInt(chars.length()));
        }
        
        return result;
    }
    
    @Override
    public void parse(String ss) {
        value = ss;
    }

    @Override
    public String toString() {
        return value;
    }
    
    @Override
    public I_DataElement clone() {
        return new StringElement(value);
    }

    @Override
    public int cmp(I_DataElement two) {
        if (!(two instanceof StringElement)) {
            return 2;
        } 
        else {
            return value.compareTo(((StringElement)two).value);
        }
    }

    @Override
    public void save(DataOutputStream out) throws IOException {
        out.writeUTF(value);
    }

    @Override
    public void load(DataInputStream in) throws IOException {
        value = in.readUTF();
    }

    @Override
    public String getName() {
        return this.getClass().getSimpleName();
    }
    
    @Override
    public String getTypeName() {
        return kStringElementType;
    }

    @Override
    public boolean equal(I_DataElement secondElement) {
        return (secondElement instanceof StringElement) 
                && ((StringElement) secondElement).value.equals(value);
    }
        
    @Override
    public void inc(I_DataElement step) {
        int length = 5;
        if (step instanceof IntegerElement) {
            length = ((IntegerElement)step).getValue();
        }
        value += generateRandomString(length);
    }
    
    @Override
    public void inc(Random step, I_DataElement MIN, I_DataElement MAX) {
        int minLength = 1;
        int maxLength = 5;
        int length = 5;
        if ((MIN instanceof IntegerElement) && MAX instanceof IntegerElement) {
            minLength = ((IntegerElement)MIN).getValue();
            maxLength = ((IntegerElement)MIN).getValue();
        }
        length = step.nextInt(maxLength) + minLength;
        value += generateRandomString(length);
    }
}
