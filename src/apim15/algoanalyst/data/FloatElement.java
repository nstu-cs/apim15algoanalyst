package apim15.algoanalyst.data;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.util.Random;

public class FloatElement extends DataElement {
    private float value;

    public FloatElement(float value) {
        this.value = value;
    }
    
    public FloatElement() {
        value = 0;
    }
        
    public float getValue() {
        return value;
    }
    
    public String toString() {
        return Float.toString(value);
    }
    
    @Override
    public void parse(String ss) {
        value = Float.parseFloat(ss);
    }

    @Override
    public I_DataElement clone() {
        return new FloatElement(value);
    }

    @Override
    public int cmp(I_DataElement two) {
        if (!(two instanceof FloatElement)) {
            return 2;
        }
        int retval = Float.compare(value, ((FloatElement)two).value);
        if (retval > 0) {
            return 1;
        }
        else if (retval < 0) {
            return -1;
        }
        else {
            return 0;
        }
    }

    @Override
    public void save(DataOutputStream out) throws IOException {
        out.writeFloat(value);
    }

    @Override
    public void load(DataInputStream in) throws IOException {
        value = in.readFloat();
    }
    
    @Override
    public String getName() {
        return this.getClass().getSimpleName();
    }

    @Override
    public String getTypeName() {
        return kFloatElementType;
    }

    @Override
    public boolean equal(I_DataElement secondElement) {
        if ((secondElement instanceof FloatElement)) {
            if (value == ((FloatElement)secondElement).value) {
                return true;
            }
            else {
                return false;
            }
        }
        else {
            return false;
        }
    }
    
    @Override
    public void inc(I_DataElement step) {
        value += ((FloatElement)step).value;
    }
    
    @Override
    public void inc(Random step, I_DataElement MIN, I_DataElement MAX) {
        float min = ((FloatElement)MIN).value;
        float max = ((FloatElement)MAX).value;
        value += step.nextFloat() * (max - min) + min;;
    }
}
