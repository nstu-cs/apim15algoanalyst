package apim15.algoanalyst.data;

import apim15.algoanalyst.I_File;
import apim15.algoanalyst.I_File;
import apim15.algoanalyst.I_TypeName;
import apim15.algoanalyst.I_TypeName;
import java.util.Random;

public interface I_DataElement extends I_File, I_TypeName {
    public void parse(String string);
    public String toString();
    public I_DataElement clone();
    public int cmp(I_DataElement secondElement);
    public boolean equal(I_DataElement secondElement); 
    public void inc(I_DataElement step);
    public void inc(Random step, I_DataElement MIN, I_DataElement MAX);
}
