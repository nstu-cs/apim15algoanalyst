package apim15.algoanalyst.data;

import apim15.algoanalyst.I_Named;
import apim15.algoanalyst.I_TypeName;
import apim15.algoanalyst.conf.I_Configurable;
import apim15.algoanalyst.conf.IllegalParameterException;
import apim15.algoanalyst.conf.ParameterFormatException;
import apim15.algoanalyst.conf.ParameterSet;
import javax.sound.midi.Sequence;

public class SequenceGenerator implements I_SequenceGenerator, I_TypeName, I_Configurable, I_Named{
    public static final String kLinearSequenceGenerator = "LinearGenerator";
    public static final String kFibonachiSequenceGenerator = "FibonachiGenerator";;
    
    @Override
    public I_DataElement getNext() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void reset() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public String getTypeName() {
        return "SequenceGenerator";
    }

    @Override
    public String getName() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public ParameterSet getParams() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void setParams(ParameterSet params) throws ParameterFormatException, IllegalParameterException {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
    
    protected I_DataElement parseDataElementOrThrow(String value, String paramName, String errorMsg) throws ParameterFormatException {
        try {
            I_DataElement data = new DataElement();
            data.parse(value);
            return data;
        } catch(NumberFormatException e) {
            throw new ParameterFormatException(errorMsg, paramName);
        }
    }
}
