package apim15.algoanalyst.data;

import apim15.algoanalyst.conf.IllegalParameterException;
import apim15.algoanalyst.conf.ParameterFormatException;
import apim15.algoanalyst.conf.ParameterSet;

public class LinearGenerator extends SequenceGenerator {
    private static final String PARAM_START = "Start value";
    private static final String PARAM_STEP = "Step value";
    
    I_DataElement start;
    I_DataElement value;
    I_DataElement step;

    public LinearGenerator() {
        this(new IntegerElement(), new IntegerElement());
    }

    public LinearGenerator(I_DataElement start, I_DataElement step) {
        this.start = start;
        this.step = step;
        value = start;
    }
   
    @Override
    public I_DataElement getNext() {
       I_DataElement val = value.clone(); 
       value.inc(step);
       return val;
    }
    
    @Override
    public void reset() {
        value = start.clone();
    }
    
   @Override
    public ParameterSet getParams() {
        ParameterSet.Parameter startParam = new ParameterSet.Parameter(PARAM_START, 
                "Start value of random generator",
                start.toString());

        ParameterSet.Parameter stepParam = new ParameterSet.Parameter(PARAM_STEP,
                "Step value of random generator",
                step.toString());
        
        return new ParameterSet()
                .add(startParam)
                .add(stepParam);
    }
    
    @Override
    public void setParams(ParameterSet params) throws ParameterFormatException, IllegalParameterException {
        for(ParameterSet.Parameter p : params.getParameters()) {
            if(p.getName().equals(PARAM_START))
                start = parseDataElementOrThrow(p.getValue(), PARAM_START, "Start values must be set");
            else if(p.getName().equals(PARAM_STEP))
                step = parseDataElementOrThrow(p.getValue(), PARAM_STEP, "Step must be set");
        }
    }



    
}
