package apim15.algoanalyst.data;

import apim15.algoanalyst.conf.*;
import apim15.algoanalyst.conf.ParameterSet.Parameter;
import java.util.Random;

public class RandomGenerator extends SequenceGenerator{
    private static final String PARAM_START = "Start value";
    private static final String PARAM_STEP = "Step value";
    private static final String PARAM_MIN = "Min value";
    private static final String PARAM_MAX = "Max value";
    
    I_DataElement MIN,MAX;
    I_DataElement start;
    I_DataElement value;
    Random step;    

    public RandomGenerator() {
        this(new FloatElement(), new FloatElement(Integer.MIN_VALUE),
                new FloatElement(Integer.MAX_VALUE));
    }

    public RandomGenerator(I_DataElement value, I_DataElement MIN, I_DataElement MAX) {
        this.step = new Random();
        this.MIN = MIN;
        this.MAX = MAX;
        this.value = value;
        start = value;
    }
   
    @Override
    public I_DataElement getNext() {
       I_DataElement val = value.clone(); 
       value.inc(step, MIN, MAX);
       return val;
    }
    
    @Override
    public void reset() {
        value = start.clone();
    }

    @Override
    public ParameterSet getParams() {
        Parameter startParam = new ParameterSet.Parameter(PARAM_START, 
                "Start value of random generator",
                start.toString());

        Parameter stepParam = new ParameterSet.Parameter(PARAM_STEP,
                "Step value of random generator",
                step.toString());
        
        Parameter minParam = new ParameterSet.Parameter(PARAM_MIN, 
                "Min possible step value",
                MIN.toString());

        Parameter maxParam = new ParameterSet.Parameter(PARAM_MAX,
                "Max possible step value",
                MAX.toString());

        return new ParameterSet()
                .add(startParam)
                .add(stepParam)
                .add(minParam)
                .add(maxParam);
    }

    @Override
    public void setParams(ParameterSet params) throws ParameterFormatException, IllegalParameterException {
        for(ParameterSet.Parameter p : params.getParameters()) {
            if(p.getName().equals(PARAM_START))
                start = parseDataElementOrThrow(p.getValue(), PARAM_START, "Start values must be set");
            else if(p.getName().equals(PARAM_MIN))
                MIN = parseDataElementOrThrow(p.getValue(), PARAM_MIN, "Min values must be set");
            else if(p.getName().equals(PARAM_MAX))
                MAX = parseDataElementOrThrow(p.getValue(), PARAM_MAX, "Max values must be set");
        }
       
    }
   
    
}
