package apim15.algoanalyst.data;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.util.Random;

public class IntegerElement extends DataElement {
    private int value;

    public IntegerElement(int value) {
        this.value = value;
    }
    
    public IntegerElement() {
        value = 0;
    }

    public int getValue() {
        return value;
    }
    
    @Override
    public void parse(String ss) {
        value = Integer.parseInt(ss);
    }
    
     @Override
    public String toString() {
        return Integer.toString(value);
    }

    @Override
    public I_DataElement clone() {
        return new IntegerElement(value);
    }

    @Override
    public int cmp(I_DataElement two) {
        if (!(two instanceof IntegerElement)) {
            return 2;
        }
        else if (value > ((IntegerElement)two).value) {
            return 1;
        }
        else if (value < ((IntegerElement)two).value) {
            return -1;
        }
        else {
            return 0;
        }
    }

    @Override
    public void save(DataOutputStream out) throws IOException {
        out.writeInt(value);
    }

    @Override
    public void load(DataInputStream in) throws IOException {
        value = in.readInt();
    }

    @Override
    public String getName() {
        return this.getClass().getSimpleName();
    }
    
    @Override
    public String getTypeName() {
        return kIntegerElementType;
    }

    @Override
    public boolean equal(I_DataElement secondElement) {
        if ((secondElement instanceof IntegerElement)) {
            if (value == ((IntegerElement)secondElement).value) {
                return true;
            }
            else {
                return false;
            }
        }
        else {
            return false;
        }
    }
    
    @Override
    public void inc(I_DataElement step) {
        value += ((IntegerElement)step).value;
    }
    
    @Override
    public void inc(Random step, I_DataElement MIN, I_DataElement MAX) {
        float min = ((IntegerElement)MIN).value;
        float max = ((IntegerElement)MAX).value;
        value += step.nextInt() * (max - min) + min;
    }
    
}
