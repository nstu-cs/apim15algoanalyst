package apim15.algoanalyst.data;

import apim15.algoanalyst.conf.IllegalParameterException;
import apim15.algoanalyst.conf.ParameterFormatException;
import apim15.algoanalyst.conf.ParameterSet;

public interface I_SequenceGenerator {
    public I_DataElement getNext();
    
    /**
     * Reset generator to it's initial state.
     * I.e. after calling this next call to getNext() will return the
     * initial element of sequence. Parameters are left intact.
     */
    public void reset();
}
