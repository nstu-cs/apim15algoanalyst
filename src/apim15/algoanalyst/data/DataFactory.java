package apim15.algoanalyst.data;

import apim15.algoanalyst.TypeFactory;

public class DataFactory extends TypeFactory<I_DataElement>{
    public DataFactory(){
        generate("apim15.algoanalyst.data", DataElement.class);
    }
}
